import java.util.ArrayList;
import java.util.ListIterator;

public class KeyStore01 {
	private ArrayList<String> liste = new ArrayList<String>();
	private int currentPos;
	public static final int MAXPOSITION = 100;
	public KeyStore01() {
		liste = new ArrayList<String> (MAXPOSITION);
		this.currentPos = 0;
	}

	public KeyStore01(int length) {
		liste = new ArrayList<String> (length);
		this.currentPos = 0;
	}

	public boolean add(String e) {

		return liste.add(e);
	}

	public void remove(int index) {
		liste.remove(index);

	}

	public int indexOf(String eintrag) {
		
		return liste.indexOf(eintrag);
	}

}
