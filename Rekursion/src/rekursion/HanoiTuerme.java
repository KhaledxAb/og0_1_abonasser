package rekursion;

public class HanoiTuerme {
	public static void towerOfHanoi(int n, char von_Turm, char nach_Turm, char hilfsTurm) {
		if (n == 1) {
			System.out.println("Scheibengröße 1 von " + von_Turm + " zu " + nach_Turm);
			return;
		}
		towerOfHanoi(n - 1, von_Turm, hilfsTurm, nach_Turm);
		System.out.println("Scheibengröße " + n + " von " + von_Turm + " zu " + nach_Turm);
		towerOfHanoi(n - 1, hilfsTurm, nach_Turm, von_Turm);
	}

	public static void main(String args[]) {
		int n = 3;
		towerOfHanoi(n, 'A', 'C', 'B');
	}

}