package oszimt;

public class AddonTest {

	public static void main(String[] args) {
		Addon tierfutter = new Addon();
		tierfutter.setIdnr(1923);
		tierfutter.setBezeichnung("Tierfutter");
		tierfutter.setVerkaufspreis(2.99);
		tierfutter.setBestanddauer(3);
		tierfutter.setMaxbestand(10);
		System.out.println(tierfutter.getBestanddauer());
		tierfutter.bestandaendern(4);
		System.out.println(tierfutter.getBestanddauer());
		System.out.println(tierfutter.getGesamtwert());

	}

}
