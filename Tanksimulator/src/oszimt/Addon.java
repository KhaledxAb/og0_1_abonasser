package oszimt;

public class Addon {

	private int id_nr;
	private String bezeichnung;
	private double verkaufspreis;
	private int bestandDauer;
	private int maxBestand;

	public Addon() {

	}

	public Addon(int id_nr, String bezeichnung, double verkaufspreis, int bestandDauer, int maxBestand) {
		id_nr = this.id_nr;
		bezeichnung = this.bezeichnung;
		verkaufspreis = this.verkaufspreis;
		bestandDauer = this.bestandDauer;
		maxBestand = this.maxBestand;

	}

	public int getIdnr() {
		return this.id_nr;

	}

	public void setIdnr(int id_nr) {
		this.id_nr = id_nr;

	}

	public String get() {
		return this.bezeichnung;

	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;

	}

	public double getVerkaufspreis() {
		return this.verkaufspreis;

	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;

	}

	public int getBestanddauer() {
		return this.bestandDauer;

	}

	public void setBestanddauer(int bestanddauer) {
		this.bestandDauer = bestanddauer;

	}

	public int getMaxbestand() {
		return this.maxBestand;

	}

	public void setMaxbestand(int maxBestand) {
		this.maxBestand = maxBestand;

	}

	public void bestandaendern(int bestandaendern) {
		this.bestandDauer = bestandaendern;
	}

	public double getGesamtwert() {
		return this.verkaufspreis * this.bestandDauer;
	}
}
