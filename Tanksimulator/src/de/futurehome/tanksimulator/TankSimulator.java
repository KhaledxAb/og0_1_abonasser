package de.futurehome.tanksimulator;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JSlider;

//import javafx.scene.control.ProgressBar;

@SuppressWarnings("serial")
public class TankSimulator extends Frame {

	public Tank myTank;

	private Label lblUeberschrift = new Label("Tank-Simulator");
	public Label lblFuellstand = new Label("     ");
	public Label lblFuellstandProzent = new Label("       ");

	public Button btnBeenden = new Button("Beenden");
	public Button btnEinfuellen = new Button("Einfüllen");
	public Button btnVerbrauchen = new Button("Verbrauchen");
	public Button btnZuruecksetzen = new Button("Zurücksetzten");
	// public ProgressBar prgProgress = new ProgressBar(4);
	public JProgressBar progressBar = new JProgressBar(0, 100);
	public JSlider jSlider = new JSlider(0, 4);

	private Panel pnlNorth = new Panel();
	private Panel pnlCenter = new Panel(new FlowLayout());
	private Panel pnlSouth = new Panel(new GridLayout(1, 0));
	// private Panel pnlEast = new Panel();

	private MyActionListener myActionListener = new MyActionListener(this);

	public TankSimulator() {
		super("Tank-Simulator");

		myTank = new Tank(0);
		this.jSlider.setMinorTickSpacing(1);
		this.jSlider.setMajorTickSpacing(4);
		this.jSlider.setPaintTicks(true);
		this.jSlider.setPaintLabels(true);
		this.lblUeberschrift.setFont(new Font("", Font.BOLD, 16));
		this.pnlNorth.add(this.lblUeberschrift);
		this.pnlNorth.add(this.progressBar);
		this.pnlCenter.add(this.lblFuellstand);
		this.pnlSouth.add(this.jSlider);
		this.pnlSouth.add(this.btnEinfuellen);
		this.pnlSouth.add(this.btnVerbrauchen);
		this.pnlSouth.add(this.btnZuruecksetzen);
		this.pnlSouth.add(this.btnBeenden);
		this.pnlCenter.add(this.lblFuellstandProzent);
		// this.pnlCenter.add(this.progressBar);
		this.add(this.pnlNorth, BorderLayout.NORTH);
		this.add(this.pnlCenter, BorderLayout.CENTER);
		this.add(this.pnlSouth, BorderLayout.SOUTH);

		this.pack();
		this.setVisible(true);

		// Ereignissteuerung
		this.btnEinfuellen.addActionListener(myActionListener);
		this.btnVerbrauchen.addActionListener(myActionListener);
		this.btnBeenden.addActionListener(myActionListener);
		this.btnZuruecksetzen.addActionListener(myActionListener);
		this.progressBar.setValue((int) myTank.getFuellstand());
		// this.jSlider.addChangeListener();

	}

	public static void main(String argv[]) {
		TankSimulator f = new TankSimulator();
		JFrame fenster = new JFrame();
		fenster.setSize(600, 400);
		fenster.setVisible(true);
		Panel pnlNorth = new Panel();
		System.currentTimeMillis();
		
	}
}