package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			double fuellstand = f.myTank.getFuellstand();

			int eingabe = f.jSlider.getValue();
			fuellstand = fuellstand + eingabe;
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText("" + fuellstand);
			f.lblFuellstandProzent.setText((fuellstand * 100) / 100 + "%");
			f.progressBar.setValue((int) f.myTank.getFuellstand());
			f.progressBar.setStringPainted(true);
		}
		if (obj == f.btnVerbrauchen) {
			int eingabe = f.jSlider.getValue();
			double fuellstand = f.myTank.getFuellstand();
			if (fuellstand > 1) {
				fuellstand = fuellstand - eingabe;
			} else {
				fuellstand = 0;
			}
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText(" " + fuellstand);
			f.lblFuellstandProzent.setText((fuellstand * 100) / 100 + "%");
			f.progressBar.setValue((int) f.myTank.getFuellstand());
			f.progressBar.setStringPainted(true);

		}
		if (obj == f.btnZuruecksetzen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand -= fuellstand;
			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstand.setText(" " + fuellstand);
			f.lblFuellstandProzent.setText((fuellstand * 100) / 100 + "%");
			f.progressBar.setValue((int) f.myTank.getFuellstand());
			f.progressBar.setStringPainted(true);
		}
	}
}