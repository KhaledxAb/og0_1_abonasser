package omnom;

public class Haustier {
	// Attribute
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;

	// Methoden
	public Haustier() {

	}

	public Haustier(String name) {
		this.name = name;
	}

	public void setHunger(int hunger) {
		if (hunger >= 0 && hunger <= 100) {
			this.hunger = hunger;
		}

	}

	public int getHunger() {
		return this.hunger;
	}

	public void setMuede(int muede) {
		if (muede >= 0 && muede <= 100) {
			this.muede = muede;
		}

	}

	public int getMuede() {
		return this.muede;
	}

	public void setZufrieden(int zufrieden) {
		if (zufrieden >= 0 && zufrieden <= 100) {
			this.zufrieden = zufrieden;
		}

	}

	public int getZufrieden() {
		return this.zufrieden;
	}

	public void setGesund(int gesund) {
		if (gesund >= 0 && gesund <= 100) {
			this.gesund = gesund;
		}

	}

	public int getGesund() {
		return this.gesund;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void fuettern(int fuetter) {
		int zwerg = this.hunger + fuetter;
		if (zwerg >= 0 && zwerg <= 100) {
			this.hunger += fuetter;
		}
		if (zwerg >= 100) {
			this.hunger = 100;
		} else {

		}

	}

	public void schlafen(int schlaf) {
		int zwerg = this.muede + schlaf;
		if (zwerg >= 0 && zwerg <= 100) {
			this.muede += schlaf;
		}
		if (zwerg >= 100) {
			this.muede = 100;
		}

		else {

		}
	}

	public void spielen(int spiel) {
		int zwerg = this.zufrieden + spiel;
		if (zwerg >= 0 && zwerg <= 100) {
			this.zufrieden += spiel;
		}
		if (zwerg >= 100) {
			this.zufrieden = 100;
		} else {

		}
	}

	public void heilen() {
		this.gesund = 100;
	}
}
