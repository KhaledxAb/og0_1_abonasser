package laufzeit;

public class FastSearch {
	public final static int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		int[] liste = { 2, 5, 7, 9, 25, 57, 65, 345 };
		int zahl = 345;
		System.out.println(fastSearch(liste, zahl));
	}

	public static int fastSearch(int[] array, int zahl) {

		int min = 0;
		int max = array.length - 1;
		while (min <= max) {
			int mb = (max + min) / 2;
			int mi = min + (max - min) * (zahl - array[min]) / (array[max] - array[min]);
			if (mb > mi) {
				int vertausche = mb;
				mb = mi;
				mi = vertausche;
				if (zahl == array[mb]) {
					return mb;

				} else if (zahl == array[mi]) {
					return mi;
				} else if (zahl < array[mb]) {
					return min = mb + 1;
				} else if (zahl < array[mi]) {
					return max = mi - 1;
				}
			}

		}
		return NICHT_GEFUNDEN;

	}

}
