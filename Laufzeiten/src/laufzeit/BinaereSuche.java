package laufzeit;

public class BinaereSuche {
	public final static int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		int[] liste = { 2, 5, 7, 9, 25, 33, 65, 345 };
		int zahl = 65;
		System.out.println(binaer(liste, zahl));
	}

	public static int binaer(int[] array, int zahl) {

		int min = 0;
		int max = array.length - 1;
		while (min <= max) {
			int m = (max + min) / 2;
			if (zahl == array[m]) {
				return m;
			} else {
				if (zahl < array[m]) {
					max = m - 1;
				} else {
					min = m + 1;
				}

			}
		}

		return NICHT_GEFUNDEN;

	}

}
