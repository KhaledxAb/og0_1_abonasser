package laufzeit;

public class MergeSort {
	private long vertauschungen = 0;
	private long vergleiche = 0;
	public static int[] hauptliste = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

	public int[] sort(int links, int rechts) {

		if (links < rechts) {
			int mitte = (links + rechts) / 2;

			sort(links, mitte);
			sort(mitte + 1, rechts);
			merge(links, mitte, rechts);
			inkrementVergleiche();

		}
		return hauptliste;
	}

	private void merge(int l, int q, int r) {
		int[] arr = new int[hauptliste.length];
		int i, j;
		for (i = l; i <= q; i++) {
			arr[i] = hauptliste[i];
			inkrementVertauschungen();
		}
		for (j = q + 1; j <= r; j++) {
			arr[r + q + 1 - j] = hauptliste[j];
			inkrementVertauschungen();

		}
		i = l;
		j = r;
		for (int k = l; k <= r; k++) {
			if (arr[i] <= arr[j]) {
				hauptliste[k] = arr[i];
				i++;

			} else {
				hauptliste[k] = arr[j];
				j--;

			}
			inkrementVertauschungen();
		}

	}

	private void inkrementVertauschungen() {
		vertauschungen++;
	}

	public long getVertauschungen() {
		return this.vertauschungen;
	}

	private void inkrementVergleiche() {
		vergleiche++;
	}

	public long getVergleiche() {
		return this.vergleiche;
	}

	public static void main(String[] args) {
		MergeSort ms = new MergeSort();
		System.out.println("Vor dem Sortieren:");
		for (int i = 0; i < ms.hauptliste.length; i++) {
			System.out.print(ms.hauptliste[i] + " ");
		}
		int[] arr = ms.sort(0, hauptliste.length - 1);
		System.out.println("\nNach dem Sortieren:");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(i + 1 + ": " + arr[i] + " ");
		}
		System.out.println("Anzahl der Vertauschungen " + ms.getVertauschungen());
		System.out.println("Anzahl der Vergleiche " + ms.getVergleiche());
	}
}