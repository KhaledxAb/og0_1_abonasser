package laufzeit;

public class BubbleSort {
	private long vertauschungen = 0;
	private long vergleiche = 0;

	public void sortiere(long[] zahlenliste) {
		int temp = 0;

		for (int n = zahlenliste.length - 1; n >= 1; n--) {
			for (int i = 0; i < n; i++) {
				if (zahlenliste[i] > zahlenliste[i + 1]) {
					temp = (int) zahlenliste[i];
					zahlenliste[i] = zahlenliste[i + 1];
					zahlenliste[i + 1] = temp;
					inkrementVertauschungen();
				}

			}
			System.out.println(array2str(zahlenliste));
		}

	}

	private void inkrementVertauschungen() {
		vertauschungen++;
	}

	public long getVertauschungen() {
		return this.vertauschungen;
	}
	private void inkrementVergleiche() {
		vergleiche++;
	}

	public long getVergleiche() {
		return this.vergleiche;
	}
	public static String array2str(long[] array) {
		String result = "";

		for (int i = 0; i < array.length; i++) {

			result = result + array[i] + "  ";

		}
		return result;
	}
	
		public static void main (String[]args){
			BubbleSort bs = new BubbleSort();
			
			long[] zahlenliste = {9,8,7,5,6,1,2,3,4};
			// {5,4,8,3,9,7,6,2,1};
			
			// Vor Sortierung
			System.out.println(array2str(zahlenliste));
			
			// Sortierung
			bs.sortiere(zahlenliste);
			
			// Nach Sortierung
			System.out.println(array2str(zahlenliste));
			System.out.println(bs.getVertauschungen());
		}
	
}