package laufzeit;

import java.util.Scanner;

public class Interpolation {
	public final static int NICHT_GEFUNDEN = -1;

	public static void main(String[] args) {
		Scanner x = new Scanner(System.in);
		long[] liste = new long[20000000];
		for (int i = 0; i < liste.length; i++) {
			liste[i] = i + 15000000;

		}
		long time = -System.currentTimeMillis();
		while (true) {
			System.out.println(interpolation(liste, x.nextLong()));
			System.out.println(time);
		}

	}

	public static int interpolation(long[] array, long zahl) {

		int min = 0;
		int max = array.length - 1;
		while (zahl >= array[min] && zahl <= array[max] && min <= max) {
			int m = (int) (min + (max - min) * (zahl - array[min]) / (array[max] - array[min]));
			if (min == max) {

				if (array[min] == zahl) {
					return min;
				} else {
					return -1;
				}
			}

			if (array[m] == zahl) {
				return m;
			}

			if (array[m] < zahl) {
				min = m + 1;
			} else {
				max = m - 1;
			}
		}

		return NICHT_GEFUNDEN;
	}
}
