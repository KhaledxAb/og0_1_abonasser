package de.oszimt.starsim2099;

import java.util.ArrayList;

import net.slashie.util.Position;

/**
 * Write a description of class Pilot here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Pilot extends Raumfahrtding {

	// Attribute
	private String grad;
	private String name;

	// Methoden
	public Pilot(double posX, double posY, String grad, String name) {
		super(posX, posY);
		this.grad = grad;
		this.name = name;
	}

	public void setGrad(String grad) {
		this.grad = grad;
	}

	public String getGrad() {
		return this.grad;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}
