package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Planet extends Raumfahrtding {

	// Attribute
	private int anzahlHafen;
	private String name;

	// Methoden
	public Planet(double posX, double posY, int anzahlHafen, String name) {
		super(posX, posY);
		this.anzahlHafen = anzahlHafen;
		this.name = name;
	}

	public void setAnzahlHafen(int anzahlHafen) {
		this.anzahlHafen = anzahlHafen;
	}

	public int getAnzahlHafen() {
		return this.anzahlHafen;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
