package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung extends Raumfahrtding {

	// Attribute
	private String typ;
	private int masse;

	// Methoden
	public Ladung(double posX, double posY, String typ, int masse) {
		super(posX, posY);
		this.typ = typ;
		this.masse = masse;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getTyp() {
		return this.typ;
	}

	public void setMasse(int masse) {
		this.masse = masse;
	}

	public int getMasse() {
		return this.masse;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}