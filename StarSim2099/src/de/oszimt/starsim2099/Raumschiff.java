package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends Raumfahrtding {

	// Attribute
	private String typ;
	private String antrieb;
	private int winkel;
	private int maxLadeKapazitaet;

	// Methoden
	public Raumschiff(double posX, double posY) {
		super(posX, posY);
	}
	public Raumschiff(double posX, double posY, String typ, String antrieb, int winkel, int maxLadeKapazitaet) {
		super(posX, posY);
		this.typ = typ;
		this.antrieb = antrieb;
		this.winkel = winkel;
		this.maxLadeKapazitaet = maxLadeKapazitaet;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getTyp() {
		return this.typ;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public String getAntrieb() {
		return this.antrieb;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	public int getWinkel() {
		return this.winkel;
	}

	public void setMaxLadekapazitaet(int maxLadeKapazitaet) {
		this.maxLadeKapazitaet = maxLadeKapazitaet;
	}

	public int getMaxLadekapazitaet() {
		return this.maxLadeKapazitaet;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { { '\0', '\0', '_', '\0', '\0' }, { '\0', '/', 'X', '\\', '\0' },
				{ '\0', '{', 'X', '}', '\0' }, { '\0', '{', 'X', '}', '\0' }, { '/', '_', '_', '_', '\\' }, };
		return raumschiffShape;
	}

}
