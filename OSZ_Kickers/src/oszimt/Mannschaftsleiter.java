package oszimt;

public class Mannschaftsleiter extends Spieler {
	private int rabatt;
	private String nameDerMannschaft;

	public Mannschaftsleiter(String name, int telefonnummer, boolean jahresbetrag, int trikotnummer,
			String spielerposition, int rabatt, String nameDerMannschaft) {
		super(name, telefonnummer, jahresbetrag, trikotnummer, spielerposition);
		this.rabatt = rabatt;
		this.nameDerMannschaft = nameDerMannschaft;
	}

	public int getRabatt() {
		return rabatt;
	}

	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}

	public String getNameDerMannschaft() {
		return nameDerMannschaft;
	}

	public void setNameDerMannschaft(String nameDerMannschaft) {
		this.nameDerMannschaft = nameDerMannschaft;
	}

}
