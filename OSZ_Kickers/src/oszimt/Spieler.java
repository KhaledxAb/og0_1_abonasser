package oszimt;

public class Spieler extends Person {
	private int trikotnummer;
	private String spielerposition;

	public Spieler(String name, int telefonnummer, boolean jahresbetrag, int trikotnummer, String spielerposition) {
		super(name, telefonnummer, jahresbetrag);
		this.trikotnummer = trikotnummer;
		this.spielerposition = spielerposition;
	}


	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielerposition() {
		return spielerposition;
	}

	public void setSpielerposition(String spielerposition) {
		this.spielerposition = spielerposition;
	}

}
