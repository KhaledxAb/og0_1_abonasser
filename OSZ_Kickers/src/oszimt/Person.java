package oszimt;

public class Person {
	private String name;
	private int telefonnummer;
	private boolean jahresbetrag;
	

	public Person(String name, int telefonnummer, boolean jahresbetrag) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbetrag = jahresbetrag;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(int telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean getJahresbetrag() {
		return jahresbetrag;
	}

	public void setJahresbetrag(boolean jahresbetrag) {
		this.jahresbetrag = jahresbetrag;
	}

}
