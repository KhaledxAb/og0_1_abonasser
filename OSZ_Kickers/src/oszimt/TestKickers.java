package oszimt;

public class TestKickers {
	public static void main(String[] args) {
		Spieler spieler01 = new Spieler("Ronaldo", 0151234123, true, 7, "Stuermer");
		System.out.println(
				"Name:" + spieler01.getName() + "! " + "Telefonnummer: " + spieler01.getTelefonnummer() + "! ");
		System.out.println("Jahresbetrag ist bezahlt: " + spieler01.getJahresbetrag() + "! " + "Trikotnummer: "
				+ spieler01.getTrikotnummer());
		System.out.println("Spielerposition: " + spieler01.getSpielerposition());
		
		Trainer trainer01 = new Trainer("Carlo Ancelotti", 0151254123, true, 'c', 350);
		System.out.println(
				"Name:" + trainer01.getName() + "! " + "Telefonnummer: " + trainer01.getTelefonnummer() + "! ");
		System.out.println("Jahresbetrag ist bezahlt: " + trainer01.getJahresbetrag() + "! " + "Lizenzklasse: "
				+ trainer01.getLizenzklasse() + " Aufwandbeschaedigung: " + trainer01.getAufwandbeschaedigung());

	}
}
