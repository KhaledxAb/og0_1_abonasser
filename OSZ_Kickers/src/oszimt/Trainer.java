package oszimt;

public class Trainer extends Person {

	private char lizenzklasse;
	private double aufwandbeschaedigung;

	public Trainer(String name, int telefonnummer, boolean jahresbetrag, char lizenzklasse, double aufwandbeschaedigung) {
		super(name, telefonnummer, jahresbetrag);
		this.lizenzklasse = lizenzklasse;
		this.aufwandbeschaedigung = aufwandbeschaedigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public double getAufwandbeschaedigung() {
		return aufwandbeschaedigung;
	}

	public void setAufwandbeschaedigung(double aufwandbeschaedigung) {
		this.aufwandbeschaedigung = aufwandbeschaedigung;
	}

}
