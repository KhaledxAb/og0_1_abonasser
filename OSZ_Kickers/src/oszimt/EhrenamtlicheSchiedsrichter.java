package oszimt;

public class EhrenamtlicheSchiedsrichter extends Person {
	private int anzahlgepfiffenerSpieler;

	public EhrenamtlicheSchiedsrichter(String name, int telefonnummer, boolean jahresbetrag,
			int anzahlgepfiffenerSpieler) {
		super(name, telefonnummer, jahresbetrag);
		this.anzahlgepfiffenerSpieler = anzahlgepfiffenerSpieler;
	}

	public int getAnzahlgepfiffenerSpieler() {
		return anzahlgepfiffenerSpieler;
	}

	public void setAnzahlgepfiffenerSpieler(int anzahlgepfiffenerSpieler) {
		this.anzahlgepfiffenerSpieler = anzahlgepfiffenerSpieler;
	}

}
