package hausaufgaben;

import java.util.Scanner;

public class Primzahl {
	public static boolean istPrimZahl(long zahl) {
		boolean istPrimZahl = true;
		if (zahl == 1 || zahl == 0) {
			istPrimZahl = false;

		} else {
			for (long i = 2; i < zahl - 1; i++) {

				if (zahl % i == 0) {
					istPrimZahl = false;
					break;
				}

				else {
					istPrimZahl = true;
				}
			}
		}
		return istPrimZahl;
	}

	public static void main(String[] args) {
		Scanner x = new Scanner(System.in);
		while (true) {
			System.out.println(istPrimZahl(x.nextLong()));
		}

	}
}
